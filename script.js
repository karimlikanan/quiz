function Question(text, choices, answer) {
    this.text = text;
    this.choices = choices;
    this.answer = answer;
}


Question.prototype.checkAnswer = function (answer) {
    return this.answer == answer;
}

function Quiz(questions) {
    this.questions = questions;
    this.score = 0;
    this.questionIndex = 0;
}

Quiz.prototype.getQuestion = function () {
    return this.questions[this.questionIndex];
}
Quiz.prototype.prvQuestion = function () {
    return this.questions[this.questionIndex - 1];
}

Quiz.prototype.isFinish = function () {
    return this.questions.length === this.questionIndex;
}


Quiz.prototype.guess = function (answer) {
    let question = this.getQuestion();

    if (question.checkAnswer(answer)) {
        this.score++;
    }
    this.questionIndex++;
}





let q1 = new Question("what's the best programming language?", ["C#", "javascript", "pyhton", "asp.net"], "javascript");

let q2 = new Question("what's the most popular language ?", ["html", "nodejs", "visual basic", "javascript"], "javascript");

let q3 = new Question("what's the most  language ?", ["c#", "javascript", "visual basic", "nodejs"], "javascript");

let q4 = new Question("what's the most   ?", ["c#", "javascript", "visual basic", "nodejs"], "javascript");


let questions = [q1, q2, q3, q4];



let quiz = new Quiz(questions);


loadQuestion();

function loadQuestion() {
    if (quiz.isFinish()) {
        showScore();
        nextBtn.style.display = "none";
        prvBtn.style.display = "none";

    } else {
        let question = quiz.getQuestion();
        let choices = question.choices;
        document.querySelector('#question').textContent = question.text;

        for (let i = 0; i < choices.length; i++) {
            let element = document.querySelector('#choice' + i);
            element.innerHTML = choices[i];

            guess('btn' + i, choices[i]);


        }
        let questionNumber = quiz.questionIndex + 1;
        showProgress(questionNumber);

    }

}



function guess(id, guess) {
    let btn = document.getElementById(id);
    btn.onclick = function () {
        quiz.guess(guess);
        loadQuestion();
     
    }
}


function showScore() {
    let html = `<h2>Score</h2><h4>${quiz.score}</h4>`;

    document.querySelector('.card-body').innerHTML = html;
}

function showProgress(questionNumber) {
    let totalQuestion = quiz.questions.length;
    // let questionNumber = quiz.questionIndex + 1;

    document.querySelector('#progress').innerHTML = 'Question ' + questionNumber + ' of ' + totalQuestion;

}
let nextBtn = document.querySelector('#nextBtn');
nextBtn.addEventListener('click', function () {
    quiz.guess(guess);
    loadQuestion();
});


let againBtn = document.querySelector('#againBtn');
againBtn.addEventListener('click', function () {
    location.reload();
})


let prvBtn = document.querySelector("#prvBtn");
prvBtn.addEventListener('click', function () {
    let prvQuestion = quiz.prvQuestion();
    let choices = prvQuestion.choices;
    questionNumber = quiz.questionIndex;
    showProgress(questionNumber);


    document.querySelector('#question').textContent = prvQuestion.text;

    for (let i = 0; i < choices.length; i++) {
        let element = document.querySelector('#choice' + i);
        element.innerHTML = choices[i];

        guess('btn' + 1, choices[i]);

    }


})

